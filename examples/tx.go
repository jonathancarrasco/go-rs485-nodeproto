package main

import (
	"log"
	"time"

	"gitlab.com/creator-makerspace/go-rs485-nodeproto"

	"github.com/tarm/serial"
)

func main() {
	c := &serial.Config{
		Name:        "/dev/ttyUSB0",
		Baud:        9600,
		ReadTimeout: time.Second * 2,
	}

	s, err := serial.OpenPort(c)
	if err != nil {
		log.Fatal(err)
	}

	nc := nodeproto.NewController(s)
	for {
		buffer := make([]byte, 128)
		nc.Transmit(nodeproto.SINGLE, 2, buffer, len(buffer), 3)
	}
}
