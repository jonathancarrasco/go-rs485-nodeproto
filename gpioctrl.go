package nodeproto

import (
	"github.com/stianeikeland/go-rpio"
	"github.com/tarm/serial"
)

// GPIOOptions - options
type GPIOOptions struct {
	RxEnablePin int
	TxEnablePin int
}

// GPIOCtrl - Interface for the controller
type GPIOCtrl interface {
	SetTransmit(loopback bool) bool
	EndTransmit()
	Init()
	End()
}

// RaspberryIOCtrl - gobot version of the ctrl
type RaspberryIOCtrl struct {
	options GPIOOptions
	s       serial.Port
	rxePin  rpio.Pin
	txePin  rpio.Pin
}

// NewRaspberryIOCtrl - Create a new controller based on Embd
func NewRaspberryIOCtrl(serialPort *serial.Port, options GPIOOptions) *RaspberryIOCtrl {
	return &RaspberryIOCtrl{
		options: options,
		s:       *serialPort,
		rxePin:  rpio.Pin(options.RxEnablePin),
		txePin:  rpio.Pin(options.TxEnablePin),
	}
}

// Init - initialize gpio etc
func (ctrl *RaspberryIOCtrl) Init() {
	rpio.Open()
	ctrl.setPinMode(rpio.Output)
	ctrl.EndTransmit()
}

// End - ends gpio etc
func (ctrl *RaspberryIOCtrl) End() {
	ctrl.setPinMode(rpio.Input)
	rpio.Close()
}

func (ctrl *RaspberryIOCtrl) setPinMode(mode rpio.Direction) {
	rpio.PinMode(ctrl.txePin, mode)

	if ctrl.options.TxEnablePin != ctrl.options.RxEnablePin {
		rpio.PinMode(ctrl.rxePin, mode)
	}
}

// SetTransmit - end current transmit
func (ctrl *RaspberryIOCtrl) SetTransmit(loopback bool) bool {
	ctrl.txePin.High()

	if !loopback && ctrl.options.TxEnablePin != ctrl.options.RxEnablePin {
		ctrl.rxePin.High()
		return false
	}

	return ctrl.options.TxEnablePin != ctrl.options.RxEnablePin
}

// EndTransmit - End trasmission
func (ctrl *RaspberryIOCtrl) EndTransmit() {
	ctrl.s.Flush()
	ctrl.txePin.Low()

	if ctrl.options.TxEnablePin != ctrl.options.RxEnablePin {
		ctrl.rxePin.Low()
	}
}
